package cn.liyunqi.findimg.demo04;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ImgTest {

    public static void main(String[] args) {

        int totalImg = 5 * 30;

        double max = totalImg / 80.0 * 100.0;
        System.out.printf("" +  max);
        /*
        String filePath = null;
        readImgWH("F:\\workspace\\FindImg\\test\\读不出来.jpg");

        readImgWH("F:\\workspace\\FindImg\\test\\刚刚好.jpg");
        readImgWH("F:\\workspace\\FindImg\\test\\太小.jpg");
        readImgWH("F:\\workspace\\FindImg\\test\\完美.jpg");
        readImgWH("F:\\workspace\\FindImg\\test\\下一半.jpg");
        */
    }

    public static void readImgWH(String path){

        System.out.println("======================================================");
        File picture = new File(path);
        try {
            System.out.println(path);
            FileInputStream is = new FileInputStream(picture);
            BufferedImage sourceImg = ImageIO.read(is);
            System.out.println(String.format("%.1f",picture.length()/1024.0));// 源图大小
            System.out.println("宽：" + sourceImg.getWidth()); // 源图宽度
            System.out.println("高：" + sourceImg.getHeight()); // 源图高度


            is.close();

            picture.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
