package cn.liyunqi.findimg;

import cn.liyunqi.findimg.controller.FindImgMainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;

public class FindImgApplication extends Application {

    public void start(Stage primaryStage) throws Exception {

        URL url = Thread.currentThread().getContextClassLoader().getResource("fxml/FindImgMain.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        Parent root = fxmlLoader.load();
        primaryStage.setResizable(true);
        primaryStage.setTitle("FindImg");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        //scene.getStylesheets().add(FindImgMainController.class.getResource("css/style.css").toExternalForm());
        primaryStage.show();
        primaryStage.getIcons().add(new Image("icons/icon.png"));

        FindImgMainController controller = fxmlLoader.getController();
        controller.setPrimaryStage(primaryStage);
                }

public static void main(String[] args){
        launch(args);
        }
        }
