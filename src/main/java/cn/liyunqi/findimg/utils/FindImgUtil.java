package cn.liyunqi.findimg.utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

public class FindImgUtil {

    private static  final double LIMIT_SIZE = 20;
    private static  final int LIMIT_WIDTH = 550;
    private static  final int LIMIT_HEIGHT = 353;

    public static void sop(Object obj){
        System.out.println(obj);
    }

    public  static String formatNumToStr(int num){
        String str = new DecimalFormat("000").format(num);
        return  str;
    }

    public static  int downloadEligibleImg(String url,String localPath){

        File file= null;
        FileOutputStream fos=null;
        HttpURLConnection httpCon = null;
        URLConnection con = null;
        URL urlObj=null;
        InputStream in =null;
        byte[] size = new byte[1024];
        int num=0;
        try {
            file = new File(localPath);
            fos = new FileOutputStream(file);
            if(url.startsWith("http")){
                urlObj = new URL(url);
                con = urlObj.openConnection();
                con.setConnectTimeout(5000);
                con.setReadTimeout(5000);
                httpCon =(HttpURLConnection) con;
                in = httpCon.getInputStream();
                while((num=in.read(size)) != -1){
                    for(int i=0;i<num;i++)
                        fos.write(size[i]);
                }
            }
        }catch (FileNotFoundException notFoundE) {
            FindImgUtil.sop("找不到该网络图片....");
        }catch(NullPointerException nullPointerE){
            FindImgUtil.sop("找不到该网络图片....");
        }catch(IOException ioE){
            FindImgUtil.sop("产生IO异常.....");
        }catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(checkEligibleImg(file) > 0){
                return 1;
            }else{
                file.delete();
                return 0;
            }


        }
    }

    public static int checkEligibleImg(File file){

        if(file == null){
            return 0;
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);

            BufferedImage sourceImg = ImageIO.read(fis);


            //先判断图片的大小尺寸
            double imgSize = file.length() / 1024.0;// 源图大小
            if(imgSize >= LIMIT_SIZE){
                //考虑到人物照片 允许竖着放
                int width = sourceImg.getWidth();// 源图宽度
                int height = sourceImg.getHeight();// 源图高度

                if((width >= LIMIT_WIDTH && height>= LIMIT_HEIGHT) || (height >= LIMIT_WIDTH && width>= LIMIT_HEIGHT)){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }catch (Exception ex){
            ex.printStackTrace();

            return 0;
        }finally {
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
