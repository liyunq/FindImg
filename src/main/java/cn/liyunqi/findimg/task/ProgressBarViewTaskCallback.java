package cn.liyunqi.findimg.task;

public interface ProgressBarViewTaskCallback {

    void complete();

    void error(Exception ex);
}
