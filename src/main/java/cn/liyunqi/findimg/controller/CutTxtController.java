package cn.liyunqi.findimg.controller;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.common.Term;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @Auther: li.yunqi
 * @Date: 2019/5/7 18:31
 * @Description: TODO
 */
public class CutTxtController extends BaseFXController{

    @FXML
    private TextArea txtCutArticle;

    private String title = "模板";

    private String titlePath = null;

    private FindImgMainController findImgMainController;

    public void setFindImgMainController(FindImgMainController findImgMainController) {
        this.findImgMainController = findImgMainController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void cutTxt(String article){

        if(article == null){
            return;
        }

        //直接提取 短语
        List<String> phraseList = HanLP.extractPhrase(article, 1);
        if(phraseList != null && phraseList.size()>0){
            title = phraseList.get(0);
        }

        //将所有的全角空格 半角空格 引号 换行符 制表符  替换
        article = article.replace("\t","").replace(" ","")
                .replace("\"","").replace("“","").replace("”","").replace("‘","")
                .replace("’","").replace("　","").replace("—","").replace("'","");;

        //将所有的 ？ ， 。 \n 替换成 空格

        //切词优先级确认 -1。  -2， -3、
        article = article.replace("?","。").replace("？","。").replace(",","，")
                .replace("，","，").replace("。","。").replace("\n","。")
                .replace("、","、").replace(";","。").replace("；","。")
                .replace("！","。").replace("!","。").replace("…","。").trim();

        //将连续两个空格以上的去除
        article = replaceDoubleSpace(article);
        //将空格替换成逗号
        article = article .replace(" ","，");
        //然后定一个最长的长度
        int limit = 21;
        int maxLimit = limit + 4;

        //作为组装的字幕str
        String text = "";


        while (!article.trim().isEmpty()){

            if(!text.isEmpty()){
                text = text + "\n";
            }

            article = article.trim();
            //如果已经是最小的了直接附上去
            if(article.length() <= limit){
                text = text + replaceDoubleSpace(formatText(article));
                article = "";
            }else{
                String temp = article.substring(0, limit);

                //寻找最后一个判断最后一个优先级 如果低级的在高级的后面，以高级的优先级为准
                int intLastFirst = temp.lastIndexOf("。");
                int intLastSecond = temp.lastIndexOf("，");
                int intLastSecond2 = temp.lastIndexOf(" ");
                int intLastThird = temp.lastIndexOf("、");

                if(intLastFirst < 0 && intLastSecond < 0 && intLastSecond2 < 0 && intLastThird < 0){
                    //特殊处理一下 极限是25，防止刚好 合格线把下一个符号去掉了
                    String tempMax = article.substring(0, maxLimit);

                    int intLastFirstMax = tempMax.lastIndexOf("。");
                    int intLastSecondMax = tempMax.lastIndexOf("，");
                    int intLastSecondMax2 = tempMax.lastIndexOf(" ");
                    int intLastThirdMax = tempMax.lastIndexOf("、");

                    if(intLastFirstMax < 0 && intLastSecondMax < 0 && intLastSecondMax2 < 0 && intLastThirdMax < 0){

                        //当什么都找不到的时候 需要进行分词 防止将同一个单次切分

                        //获取到最后一个名词 然后按名词切分
                        List<Term> segmentList = HanLP.segment(temp);

                        String lastN = "";
                        for (int i = segmentList.size() - 1; i >= 0; i--) {
                            Term term = segmentList.get(i);
                            if(term.nature != null){
                                if(term.nature.toString().startsWith("n")){
                                    lastN = term.word;
                                    break;
                                }

                            }
                        }
                        if(lastN != null && !lastN.trim().isEmpty()){
                            int index = temp.lastIndexOf(lastN);
                            if(index > 0){
                                text = text + article.substring(0,index + lastN.length())+ "";
                                article = article.substring(index + lastN.length());
                            }else{
                                text = text + temp + "**********";
                                article = article.substring(limit);
                            }
                        }else{
                            text = text + temp + "**********";
                            article = article.substring(limit);
                        }

                    }else{
                        int index = findSuitableIndex(intLastFirstMax,intLastSecondMax,intLastSecondMax2,intLastThirdMax);
                        text = text + replaceDoubleSpace(formatText(article.substring(0,index)));

                        article = article.substring(index);
                        while(article.startsWith("。") || article.startsWith("，") || article.startsWith("、")){
                            article = article.substring(1);
                        }

                    }
                }else{
                    int index = findSuitableIndex(intLastFirst,intLastSecond,intLastSecond2,intLastThird);
                    text = text + replaceDoubleSpace(formatText(article.substring(0,index)));

                    article = article.substring(index);
                    while (article.startsWith("。") || article.startsWith("，") || article.startsWith("、")){
                        article = article.substring(1);
                    }
                }
            }
        }

        txtCutArticle.setText(text);
    }
    public int findSuitableIndex(int first,int second,int second2,int third){
        if(first > 0 ){
            return first;
        }else{
            if(second > 0 || second2 > 0){
                return second > second2 ? second:second2;
            }else{
                return third;
            }
        }
    }

    public String formatText(String text){
        if(text == null || text.trim().isEmpty()){
            return "";
        }
        return text.replace("。"," ").replace("，"," ").replace("、"," ");
    }
    public String replaceDoubleSpace(String text){
        if(text == null || text.trim().isEmpty()){
            return "";
        }
        String temp = new String(text);
        while (temp.indexOf("  ")> 0){
            temp = temp.replace("  "," ").trim();
        }
        return temp.trim();
    }

    @FXML
    public void saveAsTxt(){

        String article = txtCutArticle.getText();

        if(article == null ||  article.trim().isEmpty()){
            return;
        }

        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("导出切分词");

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TXT", "*.txt"));

        if(titlePath != null){
            fileChooser.setInitialDirectory(new File(titlePath));// System.getProperty("user.home")));
        }
        fileChooser.setInitialFileName(title + ".txt");
        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            FileWriter fw = null;
            BufferedWriter bw = null;

            try {
                titlePath = file.getParentFile().getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                fw = new FileWriter(file);
                bw = new BufferedWriter(fw);

                bw.write(article);

            } catch (Exception e1) {
                e1.printStackTrace();
            }finally {
                if(bw != null){
                    try {
                        bw.close();
                        bw =null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(fw != null){
                    try {
                        fw.close();
                        fw = null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }

    }
}
