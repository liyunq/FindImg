package cn.liyunqi.findimg.controller;

import cn.liyunqi.findimg.localdb.Constants;
import cn.liyunqi.findimg.task.BaiduTask;
import cn.liyunqi.findimg.task.ProgressBarViewTaskCallback;
import cn.liyunqi.findimg.view.AlertUtil;
import com.hankcs.hanlp.HanLP;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class FindImgMainController extends BaseFXController{


    @FXML
    private ScrollPane paneSearchWord;

    @FXML
    private ScrollPane paneKeyWord;

    @FXML
    private TextArea articleText;

    private VBox cellPaneList = new VBox();

    private static String selectExportPath;

    private static AtomicBoolean isChange = new AtomicBoolean(false);

    public void initialize(URL location, ResourceBundle resources) {

        articleText.setWrapText(true);

        //检索词部分
        //先画一个大框
        VBox pane = new VBox();
        pane.setSpacing(8);
        pane.setPadding(new Insets(5,0,5,0));

        pane.setPrefWidth(270);

        //检索词框放入其中
        pane.getChildren().add(cellPaneList);

        //追加添加按钮框
        HBox addView = new HBox();
        VBox one = new VBox();
        one.setAlignment(Pos.CENTER_LEFT);
        one.setPrefWidth(180);
        one.setSpacing(4);
        one.setPadding(new Insets(0,0,0,10));

        TextField textSearch = new TextField();
        one.getChildren().add(textSearch);
        addView.getChildren().add(one);

        VBox two = new VBox();
        two.setSpacing(4);
        two.setPadding(new Insets(0,0,0,10));
        two.setAlignment(Pos.CENTER);
        Button add = new Button();
        add.setText("✔");
        add.getStyleClass().add("btn-info");
        FindImgMainController that = this;
        add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String text = textSearch.getText();
                if(text != null && !text.trim().isEmpty()){
                    addSearchWord(text,true,textSearch);
                }
            }
        });

        two.getChildren().add(add);
        addView.getChildren().add(two);

        pane.getChildren().add(addView);

        //将其和检索pane绑定
        paneSearchWord.setContent(pane);

    }

    private void addSearchWord(String searchWord, boolean needRefreshFlag, TextField textSearch){

        searchWord = searchWord == null? "": searchWord.trim();

        String finalSearchWord = searchWord;

        if(searchWord.indexOf(".") >= 0 || searchWord.indexOf("\\") >= 0 || searchWord.indexOf("/") >= 0 || searchWord.indexOf("+") >= 0 || searchWord.indexOf("*") >= 0 || searchWord.indexOf("&") >= 0){
            AlertUtil.showWarnAlert("检索词不允许使用. \\ / + * & 符号");
            return;
        }

        WarnViewConfirmHandler warnViewConfirmHandler = new WarnViewConfirmHandler() {
            @Override
            public String getWarnMsg() {
                return "警告：存在相同检索词，是否继续添加";
            }

            @Override
            public void confirm() {
                cellPaneList.getChildren().add(createSearchWordCell(finalSearchWord,cellPaneList.getChildren().size()));
                if(needRefreshFlag && textSearch != null){
                    textSearch.setText("");
                }
            }

            @Override
            public void cancel() {

            }

            @Override
            public void setChkAskAgainFlg(boolean chkAskAgainFlg, boolean always) {
                Constants.alwaysSameWord = chkAskAgainFlg;
                Constants.yesNoSameWord = always;
            }

            @Override
            public boolean getChkAskAgainShow() {
                return true;
            }
        };

        if(checkSearchWordSame(searchWord)){

            if(Constants.alwaysSameWord == false || Constants.yesNoSameWord == null){
                WarnViewController controller = (WarnViewController) loadFXMLPage("警告", FXMLPage.WarnView, false);
                controller.setFindImgMainController(this);

                controller.setWarnViewConfirmHandler(warnViewConfirmHandler);
                controller.showDialogStage();
                return;
            }else if(Constants.alwaysSameWord == true && Constants.yesNoSameWord == false){
                return;
            }else{
                warnViewConfirmHandler.confirm();
                return;
            }


        }else{
            //添加入检索词列表中
            warnViewConfirmHandler.confirm();
            return;
        }
    }

    private Boolean checkSearchWordSame(String searchWord){

        for (int i = 0; i < cellPaneList.getChildren().size(); i++) {
            VBox t = (VBox)cellPaneList.getChildren().get(i);

            HBox temp = (HBox)t.getChildren().get(0);

            VBox one = (VBox)temp.getChildren().get(0);

            Label displayLab = (Label)one.getChildren().get(one.getChildren().size() - 1);

            if(searchWord.equals(displayLab.getText())){

                return true;

            }
        }
        return false;
    }
    private void changeIndex(Node node, Integer index){

        VBox t = (VBox)node;

        HBox temp = (HBox)t.getChildren().get(0);

        VBox one = (VBox)temp.getChildren().get(0);

        Label numLab = (Label)one.getChildren().get(0);

        numLab.setText("序号："+ (index + 1));
    }

    private VBox createSearchWordCell(String searchWord ,Integer index){
        //Integer index = 0;

        //t 最外层大框 上下两层 上部复杂结构 下部 灰色分割线
        VBox t = new VBox();
        //t.setId(index + "");
        t.setSpacing(4);
        t.setPadding(new Insets(0,0,0,10));

        //复杂结构使用Hbox temp 存放 左右排列
        HBox temp = new HBox();

        //第一部分放入 序号 展示内容 隐藏的保存值内容
        VBox one = new VBox();
        one.setAlignment(Pos.CENTER_LEFT);

        one.setPrefWidth(180);
        one.setSpacing(4);
        one.setPadding(new Insets(0,10,0,0));

        Label numLab = new Label();
        numLab.setText("序号："+ (index + 1));
        one.getChildren().add(numLab);

        //展示内容嵌套了一个VBOX 因为里面的内容可能变成可入力框
        VBox showBox = new VBox();

        Label valueLab = new Label();
        valueLab.setText(searchWord);
        showBox.getChildren().add(valueLab);

        one.getChildren().add(showBox);

        Label displayLab = new Label();
        displayLab.setText(searchWord);
        displayLab.setOpacity(0);
        one.getChildren().add(displayLab);

        temp.getChildren().add(one);

        //第二部分放入可转换按钮 用来变化入力
        VBox two = new VBox();

        two.setAlignment(Pos.CENTER);

        Button updateWillBtn = new Button();
        updateWillBtn.setText("⚪");
        updateWillBtn.getStyleClass().add("btn-info");

        Button updateDidBtn = new Button();
        updateDidBtn.setText("✔");
        updateDidBtn.getStyleClass().add("btn-info");
        //updateDidBtn.setStyle("-fx-background-color: #CECEFF;");

        TextField textSearch = new TextField();

        updateWillBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //当希望编辑的时候 lab 变化成 可入力状态 同时 圆圈按钮变成打勾状态 表示 点击后保存更改

                showBox.getChildren().clear();

                textSearch.setText(displayLab.getText());
                showBox.getChildren().add(textSearch);

                two.getChildren().clear();
                two.getChildren().add(updateDidBtn);

            }
        });

        FindImgMainController that = this;

        updateDidBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                //确认点击更改后 ，如果新为空 则不做更改保存 如果新值不为空 判断是否和寄存的值相同，如果相同则 报错提示 否则进行更改
                String searchWord = textSearch.getText();

                if(searchWord.indexOf(".") >= 0 || searchWord.indexOf("\\") >= 0 || searchWord.indexOf("/") >= 0 || searchWord.indexOf("+") >= 0 || searchWord.indexOf("*") >= 0 || searchWord.indexOf("&") >= 0){
                    AlertUtil.showWarnAlert("检索词不允许使用. \\ / + * & 符号");
                    return;
                }

                if(searchWord != null && !searchWord.trim().isEmpty()){

                    searchWord = searchWord == null? "": searchWord.trim();

                    String finalSearchWord = searchWord;

                    WarnViewConfirmHandler warnViewConfirmHandler = new WarnViewConfirmHandler() {
                        @Override
                        public String getWarnMsg() {
                            return "警告：存在相同检索词，是否继续修改";
                        }

                        @Override
                        public void confirm() {
                            valueLab.setText(finalSearchWord);

                            showBox.getChildren().clear();
                            showBox.getChildren().add(valueLab);

                            displayLab.setText(finalSearchWord);

                            two.getChildren().clear();
                            two.getChildren().add(updateWillBtn);
                        }

                        @Override
                        public void cancel() {

                        }

                        @Override
                        public void setChkAskAgainFlg(boolean chkAskAgainFlg, boolean always) {
                            Constants.alwaysSameWord = chkAskAgainFlg;
                            Constants.yesNoSameWord = always;
                        }

                        @Override
                        public boolean getChkAskAgainShow() {
                            return true;
                        }
                    };

                    if(!displayLab.getText().equals(searchWord)){
                        if(checkSearchWordSame(searchWord)){

                            ////如果为空则需要询问
                            if(Constants.alwaysSameWord == false || Constants.yesNoSameWord == null){
                                WarnViewController controller = (WarnViewController) loadFXMLPage("警告", FXMLPage.WarnView, false);
                                controller.setFindImgMainController(that);

                                controller.setWarnViewConfirmHandler(warnViewConfirmHandler);
                                controller.showDialogStage();
                                return;
                            }else if(Constants.alwaysSameWord == true && Constants.yesNoSameWord == false) {
                                return;
                            }
                        }
                    }

                    warnViewConfirmHandler.confirm();
                }else{
                    AlertUtil.showErrorAlert("修改值不能为空");
                }
            }
        });

        two.getChildren().add(updateWillBtn);
        temp.getChildren().add(two);

        //第三部分放入上下按钮用于 变更上下位置 //TODO 希望后期能追加上下移动的动画效果
        VBox three = new VBox();

        three.setPadding(new Insets(0,3,0,3));

        Button upBtn = new Button();
        Button downBtn = new Button();

        upBtn.setText("↑");
        upBtn.getStyleClass().add("btn-warn");
        //upBtn.setStyle("-fx-background-color: #CECEFF;");
        upBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (!isChange.compareAndSet(false,true)){
                    System.out.println(searchWord + "========================== 等待 ↑");
                    return;
                }
                System.out.println(searchWord + "======================================================================= 同意 ↑");
                boolean hasChange = false;

                for (int i = 0; i < cellPaneList.getChildren().size(); i++) {
                    Node cell = cellPaneList.getChildren().get(i);
                    if(cell.equals(t)){
                        //如果不是最前一个则向上移动
                        if(i > 0){
                            //追加动画 并行过渡
                            try {

                                Node upCell = cellPaneList.getChildren().get(i - 1);

                                double cellY = cell.getLayoutY();
                                double upCellY = upCell.getLayoutY();
                                double y = cellY - upCellY;

                                //然后下面的cell往上移动
                                TranslateTransition ttCell = new TranslateTransition(Duration.millis(500), cell);
                                ttCell.setByY(-y);
                                ttCell.setCycleCount(1);
                                ttCell.setAutoReverse(true);

                                //然后原来上面的cell往下移动
                                TranslateTransition ttUpCell = new TranslateTransition(Duration.millis(500), upCell);
                                ttUpCell.setByY(y);
                                ttUpCell.setCycleCount(1);
                                ttUpCell.setAutoReverse(true);

                                ParallelTransition parallelTransition = new ParallelTransition();
                                parallelTransition.getChildren().addAll(
                                        ttCell,
                                        ttUpCell
                                );
                                parallelTransition.setCycleCount(1);
                                parallelTransition.setAutoReverse(true);

                                int finalI = i;
                                parallelTransition.setOnFinished(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {

                                        //sequentialTransition.

                                        //然后下面的cell往上移动
                                        TranslateTransition ttCell = new TranslateTransition(Duration.millis(1), cell);
                                        ttCell.setByY(y);
                                        ttCell.setCycleCount(1);
                                        ttCell.setAutoReverse(true);

                                        //然后原来上面的cell往下移动
                                        TranslateTransition ttUpCell = new TranslateTransition(Duration.millis(1), upCell);
                                        ttUpCell.setByY(-y);
                                        ttUpCell.setCycleCount(1);
                                        ttUpCell.setAutoReverse(true);

                                        ttCell.play();
                                        ttUpCell.play();

                                        changeIndex(upCell, finalI);
                                        changeIndex(cell, finalI - 1);

                                        cellPaneList.getChildren().remove(finalI - 1);
                                        cellPaneList.getChildren().add(finalI,upCell);

                                        System.out.println(searchWord + "========= 动画执行完成修改== 进程锁");
                                        isChange.set(false);

                                    }
                                });

                                hasChange = true;

                                parallelTransition.play();
                            }catch (Exception ex){
                                ex.printStackTrace();
                                isChange.set(false);
                            }
                        }
                        break;
                    }
                }
                if(!hasChange){
                    isChange.set(false);
                }
            }
        });



        three.getChildren().add(upBtn);

        Button mid = new Button();
        mid.setText(" ");
        mid.setOpacity(0);
        three.getChildren().add(mid);

        downBtn.setText("↓");
        downBtn.getStyleClass().add("btn-success");
        //downBtn.setStyle("-fx-background-color: #CECEFF;");
        downBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (!isChange.compareAndSet(false,true)){
                    System.out.println(searchWord + "========================== 等待 ↓");
                    return;
                }
                System.out.println(searchWord + "======================================================================= 同意 ↑");

                boolean hasChange = false;

                for (int i = 0; i < cellPaneList.getChildren().size(); i++) {
                    Node cell = cellPaneList.getChildren().get(i);
                    if(cell.equals(t)){
                        //如果不是最后一个则向下移动
                        if(i < cellPaneList.getChildren().size() - 1){
                            try {
                                Node downCell = cellPaneList.getChildren().get(i + 1);

                                double cellY = cell.getLayoutY();
                                double downCellY = downCell.getLayoutY();
                                double y = downCellY - cellY;

                                //然后上面的cell往下移动
                                TranslateTransition ttCell = new TranslateTransition(Duration.millis(500), cell);
                                ttCell.setByY(y);
                                ttCell.setCycleCount(1);
                                ttCell.setAutoReverse(true);

                                //然后原来下面的cell往上移动
                                TranslateTransition ttDownCell = new TranslateTransition(Duration.millis(500), downCell);
                                ttDownCell.setByY(-y);
                                ttDownCell.setCycleCount(1);
                                ttDownCell.setAutoReverse(true);

                                ParallelTransition parallelTransition = new ParallelTransition();
                                parallelTransition.getChildren().addAll(
                                        ttCell,
                                        ttDownCell
                                );
                                parallelTransition.setCycleCount(1);
                                parallelTransition.setAutoReverse(true);

                                int finalI = i;
                                parallelTransition.setOnFinished(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {

                                        //然后下面的cell往上移动
                                        TranslateTransition ttCell = new TranslateTransition(Duration.millis(1), cell);
                                        ttCell.setByY(-y);
                                        ttCell.setCycleCount(1);
                                        ttCell.setAutoReverse(true);

                                        //然后原来上面的cell往下移动
                                        TranslateTransition ttDownCell = new TranslateTransition(Duration.millis(1), downCell);
                                        ttDownCell.setByY(y);
                                        ttDownCell.setCycleCount(1);
                                        ttDownCell.setAutoReverse(true);

                                        ttCell.play();
                                        ttDownCell.play();

                                        changeIndex(downCell, finalI);
                                        changeIndex(cell, finalI + 1);

                                        cellPaneList.getChildren().remove(finalI + 1);

                                        cellPaneList.getChildren().add(finalI,downCell);

                                        System.out.println(searchWord + "========= 动画执行完成修改== 进程锁");
                                        isChange.set(false);

                                    }
                                });

                                hasChange = true;
                                System.out.println(searchWord + "========= 开始播放== ");

                                parallelTransition.play();
                            }catch (Exception ex){
                                ex.printStackTrace();
                                isChange.set(false);
                            }
                        }
                        break;
                    }
                }

                if(!hasChange){
                    isChange.set(false);
                }

            }
        });

        three.getChildren().add(downBtn);
        temp.getChildren().add(three);

        //第四部分用于删除按钮 //TODO 希望后期能追加删除效果
        VBox four = new VBox();

        four.setAlignment(Pos.CENTER);

        Button delBtn = new Button();
        delBtn.setText("×");
        delBtn.getStyleClass().add("btn-error");
        delBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if (!isChange.compareAndSet(false,true)){
                    System.out.println(searchWord + "========================== 等待 ×");
                    return;
                }
                System.out.println(searchWord + "======================================================================= 同意 ×");

                FadeTransition fadeTransition =
                        new FadeTransition(Duration.millis(300), t);
                fadeTransition.setFromValue(1.0f);
                fadeTransition.setToValue(0.0f);
                fadeTransition.setCycleCount(1);
                fadeTransition.setAutoReverse(true);

                fadeTransition.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        cellPaneList.getChildren().remove(t);
                        isChange.set(false);
                    }
                });
                fadeTransition.play();

            }
        });

        four.getChildren().add(delBtn);
        temp.getChildren().add(four);

        //虚线 最外层下部 放入简单分割线
        t.getChildren().add(temp);
        Separator line = new Separator();
        t.getChildren().add(line);

        return t;
    }


    @FXML
    public void getKeyWordList(){

        String article = articleText.getText();
        if(article == null || article.trim().isEmpty()){
            return;
        }
        article = article.trim();

        //直接提取 短语
        List<String> phraseList = HanLP.extractPhrase(article, 50);

        //将数据放入View里面
        if(phraseList == null || phraseList.size() == 0){
            return;
        }

        //先画一个大框
        VBox pane = new VBox();
        pane.setSpacing(8);
        pane.setPadding(new Insets(5,0,5,0));

        pane.setPrefWidth(270);

        //检索词框放入其中
        for (int i = 0; i < phraseList.size(); i++) {

            String keyWord = phraseList.get(i);

            if(keyWord != null && !keyWord.trim().isEmpty()){
                VBox cellKeyWord = createKeyWordCell(keyWord.trim());
                pane.getChildren().add(cellKeyWord);
            }
        }
        //将其和检索pane绑定
        paneKeyWord.setContent(pane);
    }

    private VBox createKeyWordCell(String keyWord){

        //t 最外层大框 上下两层 上部复杂结构 下部 灰色分割线
        VBox t = new VBox();
        //t.setId(index + "");
        t.setSpacing(4);
        t.setPadding(new Insets(0,0,0,10));

        //复杂结构使用Hbox temp 存放 左右排列
        HBox temp = new HBox();

        //第一部分放入 序号 展示内容 隐藏的保存值内容
        VBox one = new VBox();
        one.setAlignment(Pos.CENTER_LEFT);

        one.setPrefWidth(220);
        one.setSpacing(4);
        one.setPadding(new Insets(0,10,0,0));


        //展示内容嵌套了一个VBOX 因为里面的内容可能变成可入力框
        VBox showBox = new VBox();

        Label valueLab = new Label();
        valueLab.setText(keyWord);
        showBox.getChildren().add(valueLab);

        one.getChildren().add(showBox);

        temp.getChildren().add(one);

        //第二部分放入可转换按钮 用来变化入力
        VBox two = new VBox();

        two.setAlignment(Pos.CENTER);

        Button addKeyWordBtn = new Button();
        addKeyWordBtn.setText("》");
        addKeyWordBtn.getStyleClass().add("btn-success");

        addKeyWordBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                addSearchWord(keyWord,false,null);
            }
        });
        two.getChildren().add(addKeyWordBtn);
        temp.getChildren().add(two);

        //虚线 最外层下部 放入简单分割线
        t.getChildren().add(temp);
        Separator line = new Separator();
        t.getChildren().add(line);

        return t;
    }

    @FXML
    public void searchByWordsListHandle(){

        FindImgMainController that = this;

        List<String> searchWordList = new ArrayList<>();

        //获取当前列表下所有检索词
        for (int i = 0; i < cellPaneList.getChildren().size(); i++) {
            VBox t = (VBox)cellPaneList.getChildren().get(i);

            HBox temp = (HBox)t.getChildren().get(0);

            VBox one = (VBox)temp.getChildren().get(0);

            VBox showBox = (VBox)one.getChildren().get(1);

            Node node = showBox.getChildren().get(0);

            if(node instanceof Label){
                Label displayLab = (Label)one.getChildren().get(one.getChildren().size() - 1);

                searchWordList.add(displayLab.getText());

            }else{
                AlertUtil.showErrorAlert("存在未确认检索词，请先确认检索词");
                return;
            }

        }

        if(searchWordList.size() == 0){
            AlertUtil.showErrorAlert("请先设置检索词");
            return;
        }else if(searchWordList.size() > 999){
            AlertUtil.showErrorAlert("一次性最多999个检索词");
            return;
        }

        Stage fileStage = null;
        DirectoryChooser folderChooser = new DirectoryChooser();
        folderChooser.setTitle("图片导出路径");

        String path;

        try{

            File selectedFile = null;

            if(selectExportPath!= null && !selectExportPath.trim().isEmpty()){

                selectedFile = new File(selectExportPath);
                if(selectedFile.exists()){
                    folderChooser.setInitialDirectory(selectedFile);
                }
            }


            selectedFile = folderChooser.showDialog(fileStage);
            if(selectedFile == null){
                return;
            }
            path = selectedFile.getPath();
            if(path != null){
                selectExportPath = path;
                //假如选择了目标路径 则开始出图
                //开始出图
                BaiduTask baiduTask = new BaiduTask(searchWordList,selectExportPath);
                ProgressBarViewController controller = (ProgressBarViewController) loadFXMLPage("爬取图片", FXMLPage.ProgressBarView, false,baiduTask);

                baiduTask.setProgressBarViewTaskCallback(new ProgressBarViewTaskCallback() {
                    @Override
                    public void complete() {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                controller.getDialogStage().close();

                                WarnViewController controller = (WarnViewController) loadFXMLPage("提示", FXMLPage.WarnView, false);
                                controller.setFindImgMainController(that);

                                controller.setWarnViewConfirmHandler(new WarnViewConfirmHandler() {
                                    @Override
                                    public String getWarnMsg() {
                                        return "爬取完成。。~\\(≧▽≦)/~。。是否打开文件夹";
                                    }

                                    @Override
                                    public void confirm() {
                                        try {
                                            Desktop.getDesktop().open(new File(selectExportPath));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void cancel() {

                                    }
                                    @Override
                                    public void setChkAskAgainFlg(boolean chkAskAgainFlg, boolean always) {

                                    }
                                    @Override
                                    public boolean getChkAskAgainShow() {
                                        return false;
                                    }
                                });
                                controller.showDialogStage();

                            }
                        });

                    }

                    @Override
                    public void error(Exception ex) {

                    }
                });

                controller.setFindImgMainController(this);

                controller.showDialogStage();

                controller.doTask(baiduTask);

            }else{
                AlertUtil.showErrorAlert("请选择导出文件夹");
                return;
            }
        }
        catch(HeadlessException head){
            System.out.println("Open File Dialog ERROR!");
        }
    }

    @FXML
    public void cutTxt(){
        String article = articleText.getText();
        if(article == null || article.trim().isEmpty()){
            return;
        }
        article = article.trim();


        CutTxtController controller = (CutTxtController) loadFXMLPage("提示", FXMLPage.CutTxtView, false);
        controller.setFindImgMainController(this);

        controller.cutTxt(article);

        controller.showDialogStage();

    }

}
