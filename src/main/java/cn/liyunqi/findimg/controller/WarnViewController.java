package cn.liyunqi.findimg.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class WarnViewController extends BaseFXController{

    private FindImgMainController findImgMainController;

    @FXML
    private Label labWarnMsg;

    @FXML
    private CheckBox chkAskAgain;

    @FXML
    private Button confirmBtn;

    @FXML
    private Button cancelBtn;

    private WarnViewConfirmHandler warnViewConfirmHandler;

    public void initialize(URL location, ResourceBundle resources) {

    }

    void setFindImgMainController(FindImgMainController findImgMainController) {
        this.findImgMainController = findImgMainController;
    }

    void setWarnViewConfirmHandler(WarnViewConfirmHandler warnViewConfirmHandler){
        this.warnViewConfirmHandler = warnViewConfirmHandler;
        if(!warnViewConfirmHandler.getChkAskAgainShow()){
            chkAskAgain.setDisable(true);
            chkAskAgain.setOpacity(0);
        }else{
            chkAskAgain.setDisable(false);
            chkAskAgain.setOpacity(1);
        }
        confirmBtn.setText(warnViewConfirmHandler.getConfirmText());
        cancelBtn.setText(warnViewConfirmHandler.getCancelText());

        labWarnMsg.setText(warnViewConfirmHandler.getWarnMsg());
    }

    @FXML
    public void confirmHandle() {
        warnViewConfirmHandler.confirm();
        boolean chkAskAgainFlg = chkAskAgain.isSelected();

        if(warnViewConfirmHandler.getChkAskAgainShow()){
            warnViewConfirmHandler.setChkAskAgainFlg(chkAskAgainFlg,true);
        }

        getDialogStage().close();
    }

    @FXML
    public void cancelHandle() {
        warnViewConfirmHandler.cancel();
        boolean chkAskAgainFlg = chkAskAgain.isSelected();

        if(warnViewConfirmHandler.getChkAskAgainShow()){
            warnViewConfirmHandler.setChkAskAgainFlg(chkAskAgainFlg,false);
        }

        getDialogStage().close();
    }

}
