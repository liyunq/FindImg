package cn.liyunqi.findimg.controller;

public abstract class WarnViewConfirmHandler {

    public abstract String getWarnMsg();

    public abstract void confirm();

    public abstract void cancel();

    public abstract void setChkAskAgainFlg(boolean chkAskAgainFlg,boolean always);

    public abstract boolean getChkAskAgainShow();

    public String getConfirmText(){
        return "确认";
    }

    public String getCancelText(){
        return "取消";
    }
}
