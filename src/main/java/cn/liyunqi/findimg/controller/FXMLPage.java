package cn.liyunqi.findimg.controller;

/**
 * FXML User Interface enum
 * <p>
 * Created by Owen on 6/20/16.
 */
public enum FXMLPage {

    FindImgMain("fxml/FindImgMain.fxml"),
    WarnView("fxml/WarnView.fxml"),
    CutTxtView("fxml/CutTxt.fxml"),
    ProgressBarView("fxml/ProgressBarView.fxml");


    private String fxml;

    FXMLPage(String fxml) {
        this.fxml = fxml;
    }

    public String getFxml() {
        return this.fxml;
    }
}
