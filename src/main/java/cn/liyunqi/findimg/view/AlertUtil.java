package cn.liyunqi.findimg.view;


import javafx.application.Platform;
import javafx.scene.control.Alert;

/**
 * Created by liyunqi on 20180622.
 */
public class AlertUtil {

    public static void showInfoAlert(String message) {

        final String message1 = message;

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText(message1);
                alert.show();
            }
        });
    }

    public static void showWarnAlert(String message) {
        final String message1 = message;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText(message1);
                alert.show();
            }
        });

    }

    public static void showErrorAlert(String message) {
        final String message1 = message;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText(message1);
                alert.show();
            }
        });

    }

    /**
     * build both OK and Cancel buttons for the user
     * to click on to dismiss the dialog.
     *
     * @param message
     */
    public static Alert buildConfirmationAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText(message);
        return alert;
    }

}
