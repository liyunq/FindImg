# FindImg

![输入图片说明](./img/icon.png "图标.png")

#### 项目介绍以及主要功能

利用百度图片接口爬取想要的图片

#### 技术选型
1. javaFX 用于绘制桌面应用程序
2. jsoup 解析爬取图片的文本
3. hanlp 用于中文切词

#### 环境搭建
1. 开发环境为jdk1.8，基于maven构建；
2. 使用eclipase或Intellij Idea开发(推荐使用Intellij Idea)
#### 下载地址：
（可直接下载安装使用，本地无需要有jdk1.8环境）
[find-img](https://gitee.com/liyunq/FindImg/releases)
#### 项目结构
```
find-img
├─ img	项目用到的资源文件
├─ src
│  ├─ main
│  │  ├─ java
│  │  │  └─ cn
│  │  │   └─ liyunqi
│  │  │    └─ findimg
│  │  │     ├─ controller   javafx控制层
│  │  │     ├─ localdb  存放了用到的全局常量
│  │  │     ├─ task	执行爬取任务时提出来的baidu共通
│  │  │     ├─ utils	抽取出来的工具类包
│  │  │     ├─ view	抽取出来用于简单弹框的实现
│  │  │     ├─ FindImgApplication	启动类
│  │  └─ resources
│  │   ├─ css 存放了页面上抽取的样式库
│  │   ├─ fxml	页面资源文件
│  │   ├─ icons	应用程序用到的图标
│  └─ test  测试类
│  │  ├─ java
│  │  │  └─ cn
│  │  │   └─ liyunqi
│  │  │    └─ findimg 里面放了开发过程中的一些测试样例 
```
#### 打包成直接可执行的jar的方式

1. 在项目根目录下直接运行
```
mvn package assembly:single
```

#### 程序展示

![输入图片说明](./img/step-1.gif "传输工具.png")
![输入图片说明](./img/step-2.gif "传输工具.png")
![输入图片说明](./img/step-3.gif "传输工具.png")
![输入图片说明](./img/step-4.gif "传输工具.png")

#### 给这位小老弟倒杯卡布奇诺

![微信扫一扫](./img/money.jpg "微信扫一扫.png")